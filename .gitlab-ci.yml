---

variables:
  MAJOR: 4
  DOCKER_IMAGE: $CI_REGISTRY_IMAGE/tmp/$SCANNER:$CI_COMMIT_SHA
  DEPLOY_REGISTRY_IMAGE: registry.gitlab.com/security-products/container-scanning
  SCANNER: trivy
  DEFAULT_SCANNER: trivy
  GIT_STRATEGY: fetch

include:
  - template: Security/Container-Scanning.gitlab-ci.yml
  - template: Security/Dependency-Scanning.gitlab-ci.yml
  - template: Security/License-Scanning.gitlab-ci.yml
  - template: Security/SAST.gitlab-ci.yml
  - local: .gitlab/maintenance.yml


# Workaround to the detached pipeline as described in
# https://gitlab.com/gitlab-org/gitlab/-/issues/34756
workflow:
  rules:
    - if: '$CI_MERGE_REQUEST_EVENT_TYPE == "detached"'
      when: never
    - when: always

stages:
  # check, test, and scan the Docker images
  - initial-test
  - build-image
  - test
  # release Docker images and distro packages
  - release-version
  # update Docker images and distro packages of the major release
  - release-major
  # for scheduled pipeline, we release same image everyday to keep
  # vulnerability db updated
  - maintenance

.not-on-schedule:
  rules:
    - if: '$CI_PIPELINE_SOURCE == "schedule"'
      when: never
    - if: '$SCANNER == "grype"'
      allow_failure: true
    - if: '$CI_PIPELINE_SOURCE != "schedule"'
      when: on_success

.ruby-alpine:
  extends: .not-on-schedule
  image: ruby:3.0.1-alpine
  before_script:
    - apk add git build-base
    - bundle config --local jobs "$(nproc)"
    - bundle config set no-cache 'true'
    - bundle install --quiet

# Yamllint of CI-related yaml and changelogs.
lint-yaml:
  extends: .not-on-schedule
  image: pipelinecomponents/yamllint:latest
  stage: initial-test
  variables:
    LINT_PATHS: .gitlab-ci.yml .rubocop.yml spec/fixtures
  script:
    - yamllint -c .yamllint -f colored $LINT_PATHS

shellcheck:
  extends: .not-on-schedule
  stage: initial-test
  image: koalaman/shellcheck-alpine:stable
  script:
    - shellcheck -e SC1071 script/*

check-commit-message:
  extends: .ruby-alpine
  stage: initial-test
  script:
    - bundle exec rake commit_message
  allow_failure: true

unit test:
  extends: .ruby-alpine
  stage: initial-test
  script:
    - bundle exec rake unit_test

gitlab styles:
  extends: .ruby-alpine
  stage: initial-test
  script:
    - bundle exec rubocop

dependency_scanning:
  stage: initial-test

gemnasium-dependency_scanning:
  rules:
    - if: '$CI_PIPELINE_SOURCE == "schedule"'
      when: never
    - changes:
      - Gemfile.lock

bundler-audit-dependency_scanning:
  rules:
    - if: '$CI_PIPELINE_SOURCE == "schedule"'
      when: never
    - changes:
      - Gemfile.lock

license_scanning:
  stage: initial-test
  rules:
    - if: '$CI_PIPELINE_SOURCE == "schedule"'
      when: never
    - changes:
      - Gemfile.lock
      when: manual

.build-tmp-image:
  extends: .not-on-schedule
  image: docker:stable
  stage: build-image
  services:
    - docker:19.03.5-dind
  script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker build --build-arg SCANNER -q -t $DOCKER_IMAGE .
    - docker push $DOCKER_IMAGE

.scanners-matrix:
  parallel:
    matrix:
      - SCANNER: [trivy, grype]

build-scanner-image:
  extends:
    - .build-tmp-image
    - .scanners-matrix

.integration_test:
  extends:
    - .not-on-schedule
    - .scanners-matrix
  image: $DOCKER_IMAGE
  cache:
    key: ${CI_COMMIT_REF_SLUG}
    paths:
      - vendor/ruby
  stage: test
  variables:
    IMAGE_TAG: $DOCKER_IMAGE
    CS_QUIET: "true"
  script:
    - sudo ./script/setup_integration
    - bundle config set path 'vendor/ruby'
    - bundle install --quiet
    - bundle exec rake $SPEC_NAME

alpine:
  variables:
    SPEC_NAME: spec_integration_alpine
  extends: .integration_test

centos:
  variables:
    SPEC_NAME: spec_integration_centos
  extends: .integration_test

webgoat:
  variables:
    SPEC_NAME: spec_integration_webgoat
  extends: .integration_test

ca cert:
  variables:
    SPEC_NAME: spec_integration_ca_cert
  extends: .integration_test

.docker_tag:
  extends:
    - .not-on-schedule
    - .scanners-matrix
  image: docker:stable
  stage: release-version
  services:
    - docker:19.03.5-dind
  script:
    - docker info
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - export SOURCE_IMAGE=$DOCKER_IMAGE
    - docker pull $SOURCE_IMAGE
    - |
      if [ "$DEFAULT_SCANNER" = "$SCANNER" ]; then
        TARGET_IMAGE=$CI_REGISTRY_IMAGE:${IMAGE_TAG:-$CI_JOB_NAME}
        docker tag $SOURCE_IMAGE $TARGET_IMAGE
        docker push $TARGET_IMAGE
      fi
    - |
      # code below only runs on protected branches/tags
      if [ -n "$CS_DEPLOY_USERNAME" ] && [ -n "$CS_DEPLOY_PASSWORD" ] && [ -n "$IMAGE_TAG" ]; then
        docker login -u "$CS_DEPLOY_USERNAME" -p "$CS_DEPLOY_PASSWORD" "$DEPLOY_REGISTRY_IMAGE"
        if [ "$DEFAULT_SCANNER" = "$SCANNER" ]; then
          TARGET_IMAGE_DEFAULT="${DEPLOY_REGISTRY_IMAGE}:${IMAGE_TAG}"
          docker tag "$SOURCE_IMAGE" "$TARGET_IMAGE_DEFAULT"
          docker push "$TARGET_IMAGE_DEFAULT"
        fi
        TARGET_IMAGE_SCANNER="${DEPLOY_REGISTRY_IMAGE}/${SCANNER}:${IMAGE_TAG}"
        docker tag "$SOURCE_IMAGE" "$TARGET_IMAGE_SCANNER"
        docker push "$TARGET_IMAGE_SCANNER"
      fi

tag branch:
  extends: .docker_tag
  variables:
    # CAUTION: by preferring `SLUG` over `NAME` we can properly handle
    # non-alphanumeric characters, but this may limit our tags to 63 chars
    # or raise potential conflicts.
    IMAGE_TAG: $CI_COMMIT_REF_SLUG
  rules:
    - if: $CI_DEFAULT_BRANCH == $CI_COMMIT_REF_NAME
      when: never
    - if: $CI_COMMIT_BRANCH

tag edge:
  extends: .docker_tag
  variables:
    IMAGE_TAG: edge
  rules:
    - if: $CI_DEFAULT_BRANCH == $CI_COMMIT_REF_NAME

tag version:
  extends: .docker_tag
  before_script:
    - apk add ruby
    - export IMAGE_TAG=${CI_COMMIT_TAG/v/}
    - ruby ./script/check_version
  rules:
    - if: $CI_COMMIT_TAG

.release:
  extends: .docker_tag
  stage: release-major
  rules:
    - if: $CI_COMMIT_TAG

major:
  extends: .release
  variables:
    IMAGE_TAG: $MAJOR

latest:
  extends: .release
  variables:
    IMAGE_TAG: latest
